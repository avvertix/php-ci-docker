
# PHP Docker Images to run composer based projects

This repository contains the Dockerfile to build PHP-CLI images based on version 5.6, 7.0.

The image comes pre-loaded with

- git command
- composer command
- xdebug
- iconv
- mcrypt
- gd
- zip
- exif
- pdo_mysql
- envsubst command

In this way can be used with [Gitlab CI](http://docs.gitlab.com/ce/ci/docker/using_docker_images.html)

**Important: xdebug is available only on PHP 5.6 and 7.0**

## Usage

Images are hosted on the Gitlab Docker Registry

```
registry.gitlab.com/avvertix/php-ci-docker
```

To pull an image use

```
docker pull registry.gitlab.com/avvertix/php-ci-docker:{tag}
```

Available {tag}s are:

- `5.6` (based on the official `php:5.6-cli`)
- `5.6-alpine` (based on the official `php:5.6-alpine`) **no xdebug on this image**
- `7.0` (based on the official `php:7.0-cli`)




### Important usage notices

- **You need to have PHPUnit in your development dependencies because is not included by default**


## Build

```
docker build -t {image-name} ./{version-folder} 
```

where 

- {image-name} is the name of the image you want to use
- {version-folder} is the folder corresponding to the PHP version you want to use

In this repository the version folder always follows the pattern `Major.Minor`, e.g. the folder for php 5.6 is `5.6`.